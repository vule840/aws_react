import './App.css';
import { withAuthenticator, Button, Heading } from '@aws-amplify/ui-react';
import '@aws-amplify/ui-react/styles.css';
import { useEffect } from 'react';

/* src/App.js */
function App({ signOut, user }) {
  // Todo logic here
  useEffect (() => {
    console.log(window.document.cookie);
    if (window.document.cookie) {
      // window.location.reload();
    } 

  }, []);
  return (
    <>
      {/* Add Todo JSX here  */}
      <Heading level={1}>Hello {user.username}</Heading>
      <Button onClick={signOut}>Sign out</Button>
    </>
  );
}

export default withAuthenticator(App);